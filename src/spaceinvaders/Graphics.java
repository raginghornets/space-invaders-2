/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 *
 * @author zeus
 */
public class Graphics {
    private final GraphicsContext ctx;

    public Graphics(GraphicsContext ctx) {
        this.ctx = ctx;
    }
    
    public void drawBackground(int width, int height, Color color) {
        ctx.setFill(color);
        ctx.fillRect(0, 0, width, height);
    }
    
    @SuppressWarnings("NonPublicExported")
    public void drawGameObject(GameObject obj) {
        ctx.drawImage(obj.img(), obj.pos()[0], obj.pos()[1], obj.scl()[0], obj.scl()[1]);
    }
    
}
