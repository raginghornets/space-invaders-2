/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import javafx.scene.image.Image;

/**
 *
 * @author zeus
 */
public class Ship extends GamePiece {
    
    private static final Image img = new Image("file:ship.png");
    private int score, lives;
    
    public Ship(int[] pos, int[] scl) {
        super(pos, scl, img);
        score = 0;
        lives = 3;
    }
    
    @Override
    public void die() {
        if(lives > 0) {
            lives -= 1;
        }
    }

    @Override
    public void shoot() {
        if(projectile.velocity[1] != -1) {
            projectile.position[0] = position[0] + scale[0] / 2 - projectile.scale[0] / 2;
            projectile.velocity[1] = -1;
        }
    }
    
    public void resetProjectile() {
        projectile.velocity[1] = 0;
        projectile.position[1] = position[1];
    }
    
    public void addScore(int sc) {
        score += sc;
    }
    
    public int getScore() {
        return score;
    }
    
    public int getLives() {
        return lives;
    }
    
}
