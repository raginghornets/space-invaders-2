/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package spaceinvaders;

import javafx.scene.image.Image;

/**
 *
 * @author zeus
 */
public class Movable extends GameObject {
    protected int[] velocity;
    protected int speed;
    
    public Movable(int[] pos, int[] scl, Image img) {
        super(pos, scl, img);
        velocity = new int[] {0, 0};
        speed = 5;
    }
    
    /**
     * Move the movable according to its velocity multiplied by its speed
     */
    public void move() {
        position[0] += velocity[0] * speed;
        position[1] += velocity[1] * speed;
    }
    
    /**
     * @return the velocity of the Movable
     */
    public int[] vel() {
        return velocity;
    }
    
    public void setVelocity(int vel) {
        velocity[0] = vel;
    }
}
